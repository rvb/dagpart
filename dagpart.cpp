#include "graph.hpp"
#include "randgraphs.hpp"
#include <cassert>
#include <getopt.h>
#include <cstdlib>
#include <iostream>
#include <functional>
#include <limits>
#include <iomanip>
#include <vector>
#include <boost/graph/topological_sort.hpp>

struct timeout_error {
  timeout_error(int s) : secs(s) {}
  int secs;
};

using namespace std;

int foundk = numeric_limits<int>::max();
int timeout = 400;
int maxdepth = 0;
int mindepth = 0;
int starttime = 0;

float runtime() {
  return (((float)clock())-starttime)/CLOCKS_PER_SEC;
}

void stats_time(const char* heading, const char* secstr, const function<void ()> &func) {
  try {
    cout << endl << "---- " << heading << " ----" << endl;
    clock_t t = clock();
    func();
    t = clock() - t;
    cout << secstr << ": " << ((float)t)/CLOCKS_PER_SEC << endl;
  } catch (timeout_error& err) {
    cout << secstr << ": TIMEOUT " << err.secs << "s" << endl;
  }
}

list<v_t> abovesinks(const graph_t &g) {
  list<v_t> res;
  
  v_iter vs, ve;
  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs) {
    if(issink(*vs, g)) continue;
    out_iter es, ee;
    tie(es, ee) = out_edges(*vs, g);
    
    for(; es != ee; ++es) {
      if(!issink(target(*es, g), g)) goto noninteresting;
    }
    res.push_back(*vs);
  noninteresting:
    ;
  }
  
  return res;
}

int max_conn_heuristic(graph_t g) {
  int k = 0;

  while(true) {
    reduce_graph(g);
    cout << setw(10) << runtime() << " Data reduction: ("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << k << endl;
    if(num_vertices(g) == 0) return k;

    list<v_t> asinks(abovesinks(g));
    
    cout << "\t\t" << asinks.size() << " vertices above sinks" << endl;
    for(auto v = asinks.begin(); v != asinks.end(); ++v) {
      out_iter es, ee;
      
      tie(es, ee) = out_edges(*v, g);
      e_t bestedge = *es;
      for(; es != ee; ++es) 
	if(g[*es].w > g[bestedge].w) bestedge = *es;
    
      tie(es, ee) = out_edges(*v, g);
      for(; es != ee; ++es)
	if(*es != bestedge) {
	  k+=g[*es].w;
	  g[*es].deleted = true;
	}
    }
  }
}

int max_conn_heuristic_noDR(const graph_t &g) {
  int deleted = 0;
  vector<int> in_cluster(num_vertices(g), 0);

  list<v_t> sorted;
  topological_sort(g, back_inserter(sorted), vertex_index_map(get(&vertinfo::idx, g)));

  auto cvert = sorted.begin();

  int curclu = 0;
  for(; cvert != sorted.end(); ++cvert) {
    if(issink(*cvert, g)) {
      in_cluster[g[*cvert].idx] = curclu++;
      continue;
    }

    vector<int> to_cluster(curclu, 0);
    out_iter es, ee;

    tie(es, ee) = out_edges(*cvert, g);
    for(; es != ee; ++es) to_cluster[in_cluster[g[target(*es, g)].idx]]+= g[*es].w;
      
    tie(es, ee) = out_edges(*cvert, g);
    int bestclust = 0;
    for(; es != ee; ++es) 
      if(to_cluster[in_cluster[g[target(*es, g)].idx]] > to_cluster[bestclust])
	bestclust = in_cluster[g[target(*es, g)].idx];
    
    tie(es, ee) = out_edges(*cvert, g);
    for(; es != ee; ++es)
      if(in_cluster[g[target(*es, g)].idx] != bestclust)
	deleted += g[*es].w;
  
    in_cluster[g[*cvert].idx] = bestclust;
    //    cout << g[*cvert].idx << " to cluster " << bestclust << endl;
  }

  return deleted;
}

int max_conn_heuristic_withDR(graph_t g) {
  int deleted = 0;

  reduce_graph(g);
  cout << setw(10) << runtime() << " Data reduction: ("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << deleted << endl;

  while(num_vertices(g) != 0) {
    vector<int> in_cluster(num_vertices(g), 0);
    list<v_t> sorted;
    topological_sort(g, back_inserter(sorted), vertex_index_map(get(&vertinfo::idx, g)));

    auto cvert = sorted.begin();

    int curclu = 0;
    for(; cvert != sorted.end(); ++cvert) {
      if(issink(*cvert, g)) {
	in_cluster[g[*cvert].idx] = curclu++;
	continue;
      }
      
      vector<int> to_cluster(curclu, 0);
      out_iter es, ee;
      
      tie(es, ee) = out_edges(*cvert, g);
      for(; es != ee; ++es) to_cluster[in_cluster[g[target(*es, g)].idx]]+= g[*es].w;
      
      tie(es, ee) = out_edges(*cvert, g);
      int bestclust = 0;
      for(; es != ee; ++es) 
	if(to_cluster[in_cluster[g[target(*es, g)].idx]] > to_cluster[bestclust])
	  bestclust = in_cluster[g[target(*es, g)].idx];
      
      tie(es, ee) = out_edges(*cvert, g);
      for(; es != ee; ++es)
	if(in_cluster[g[target(*es, g)].idx] != bestclust) {
	  deleted += g[*es].w;
	  g[*es].deleted = true;
	}
      
      in_cluster[g[*cvert].idx] = bestclust;
      break;
    }
    reduce_graph(g);
    cout << setw(10) << runtime() << " Data reduction: ("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << deleted << endl;
  }
  return deleted;
}


bool start_st(const graph_t &g, list<v_t>::iterator vs, 
	      list<v_t>::iterator ve, vector<int> &in_cluster,
	      int curclu, int maxk, int k = 0, int depth = 0) {
  if(depth > maxdepth) {
    if(mindepth >= maxdepth)
      mindepth = depth;
    maxdepth = depth;
  }
  
  if(depth < mindepth) {
    cout << setw(10) << runtime() << " remaining search tree levels: "
	 << mindepth << endl;
    mindepth = depth;
    
    if(((clock() - starttime)/CLOCKS_PER_SEC) > timeout)
      throw timeout_error(((clock() - starttime)/CLOCKS_PER_SEC));
  }
  
  if(k > foundk) return false;

  list<int> try_clusters;
  out_iter es, ee;

  while(try_clusters.size() <= 1) {
    while(vs != ve && issink(*vs, g)) {
      in_cluster[g[*vs].idx] = curclu++;
      ++vs;
    }
    
    if(vs == ve) {
      if(k < foundk) {
	cout << setw(10) << runtime() << " Upper bound " << k << endl;
	foundk = k;
      }
      return true;
    }
    
    vector<int> to_cluster(curclu, 0);
    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es) {
      if(!to_cluster[in_cluster[g[target(*es, g)].idx]]) {
	try_clusters.push_back(in_cluster[g[target(*es, g)].idx]);
	to_cluster[in_cluster[g[target(*es, g)].idx]] = true;
      }
    }
    
    if(try_clusters.size() <= 1) {
      auto i = *(try_clusters.begin());
      in_cluster[g[*vs].idx] = i;
      vs++;
      try_clusters.clear();
    }
  }

  if(k + try_clusters.size() - 1 > foundk) return false;
  
  bool sol = false;
  for(auto  &i : try_clusters) {
    int add_deleted = 0;
    
    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es)
      if(in_cluster[g[target(*es, g)].idx] != i)
	add_deleted += g[*es].w;
    
    in_cluster[g[*vs].idx] = i;
    sol |= start_st(g, next(vs), ve, in_cluster, curclu,
		    maxk, k + add_deleted, depth + 1);
  }
  return sol;
}


int search_tree_noDR(const graph_t &g, int maxk, int k = 0, int depth = 0) {
  if(k > foundk) return false;

  if(depth == 0) {
    foundk = maxk;
    maxdepth = 0;
    mindepth = 0;
    starttime = clock();
    cout << setw(10) << runtime() << " Upper bound " << foundk << endl;
  }
  
  vector<int> in_cluster(num_vertices(g), 0);

  list<v_t> sorted;
  topological_sort(g, back_inserter(sorted), vertex_index_map(get(&vertinfo::idx, g)));

  auto vs = sorted.begin();
  int curclu = 0;

  return start_st(g, vs, sorted.end(), in_cluster, curclu, maxk, k, depth);
}


int stupid_heuristic(graph_t g) {
  int k = 0;

  while(true) {
    reduce_graph(g);
    cout << setw(10) << runtime() << " Data reduction: ("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << k << endl;
    if(num_vertices(g) == 0) return k;

    list<v_t> asinks(abovesinks(g));
    
    cout << "\t\t" << asinks.size() << " vertices above sinks" << endl;
    for(auto v = asinks.begin(); v != asinks.end(); ++v) {
      out_iter es, ee;

      tie(es, ee) = out_edges(*v, g);
      e_t firstedge = *es;
      for(; es != ee; ++es)
	if(*es != firstedge) {
	  k+=g[*es].w;
	  g[*es].deleted = true;
	}
    }
  }
}


bool search_tree(graph_t g, int maxk, int k, int depth);

bool search_tree_noreduce(graph_t &g, list<v_t>::iterator vs,
			  list<v_t>::iterator ve, int maxk,
			  int k = 0, int depth = 0) {
  if(depth > maxdepth) {
    if(mindepth >= maxdepth)
      mindepth = depth;
    maxdepth = depth;
  }

  if(depth < mindepth) {
    cout << setw(10) << runtime() << " remaining search tree levels: " << mindepth << endl;
    mindepth = depth;

    if(((clock() - starttime)/CLOCKS_PER_SEC) > timeout)
      throw timeout_error(((clock() - starttime)/CLOCKS_PER_SEC));
  }

  if(k > foundk) return false;
  if(vs == ve) return search_tree(g, maxk, k, depth);
  if(k + out_degree(*vs, g) - 1 > foundk) return false;
  
  out_iter pivot, pivote;
  tie(pivot, pivote) = out_edges(*vs, g);
  bool res = false;
  for(; pivot != pivote; ++pivot) {
    out_iter es, ee;
    int kdiff = 0;

    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es)
      if(*es != *pivot) {
	g[*es].deleted = true;
	kdiff += g[*es].w;
      }

    res |= search_tree_noreduce(g, next(vs), ve, maxk,
				k + kdiff, depth + 1);

    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es)
      if(*es != *pivot) g[*es].deleted = false;
  }
  return res;
}

bool search_tree(graph_t g, int maxk, int k = 0, int depth = 0) {
  if(k > foundk) return false;

  if(depth == 0) {
    foundk = maxk;
    maxdepth = 0;
    mindepth = 0;
    starttime = clock();
    cout << setw(10) << runtime() << " Upper bound " << foundk << endl;
  }

  reduce_graph(g);

  if(num_vertices(g) == 0) {
    if (k<foundk) cout << setw(10) << runtime() << " Upper bound " << k << endl;

    foundk = k;
    return true;
  }

  list<v_t> asinks(abovesinks(g));
  return search_tree_noreduce(g, asinks.begin(), asinks.end(), maxk, k, depth + 1);
}

void usage(const char* cmd) {
  cout << "---------------------------------------------------------"
       << endl << "Solver for DAG Partitioning(C)"
       << endl << "2013 René van Bevern <rene.vanbevern@tu-berlin.de>"
       << endl
       << endl << "The program is distributed under the terms and"
       << endl << "conditions of the GNU General Public Licence and comes"
       << endl << "with absolutely NO WARRANTY" 
       << endl << "---------------------------------------------------------"
       << endl << endl;


  cout << cmd << " [-p out.dot] <operation>,"
       << endl << "    where <operation> is one of"
       << endl
       << endl << "-f file -k k: search for partitioning set of size k in file"
       << endl
       << endl << "-K -n n -c c -k k: search for partitioning set of size k in"
       << endl << "  graph consisting of c complete connected components, each"
       << endl << "  with n vertices, interconnected by k random arcs"
       << endl
       << endl << "-E -n n -c c -k k: search for partitioning set of size k in"
       << endl << "  graph consisting of c Erdos-Renyi random graphs, each"
       << endl << "  with n vertices and edge probability 0.5, interconnected"
       << endl << "  by k random arcs"
       << endl
       << endl << "-A -n n -c c -k k -o o: search for partitioning set of size"
       << endl << "  k in graph consisting of c preferential attachment graphs"
       << endl << "  with outdegree o, each with n vertices, interconnected by"
       << endl << "  k random arcs" 
       << endl
       << endl << "-C -n n -c c -o o: search for partitioning set of size k in"
       << endl << "  preferential attachment graph with outdegree o and"
       << endl << "  c sinks."
       << endl
       << endl << "If the option '-p out.dot' is used, the generated graph is"
       << endl << "written to the file out.dot in graphviz format" << endl;
}

int main (int argc, char *argv[]) {
  int c = 1;
  int inc;
  int n = 2;
  int k = 0;
  int o = 0;
  bool C = 0;
  int heurres;
  char *p = 0;
  bool K = 0;
  bool E = 0;
  bool A = 0;
  graph_t g;

  while((inc=getopt(argc, argv, "t:k:f:p:n:c:o:CKEA")) != -1)
    switch (inc) {
    case 'f': stats_time("Reading from file", "read seconds", [&] () {g = graph_read(optarg); }); break;
    case 'K': K = true; break;
    case 'E': E = true; break;
    case 'A': A = true; break;
    case 'C': C = true; break;
    case 'c': c = atoi(optarg); assert(c>=1); break;
    case 'n': n = atoi(optarg); assert(n>=1); break;
    case 'o': o = atoi(optarg); assert(o>=1); break;
    case 't': timeout = atoi(optarg); assert(timeout>=1); break;
    case 'g': 
    case 'k':
      k = atoi(optarg);
      assert(k >= 0);
      break;
    case 'p':
      p = optarg;
      break;
    default:
      usage(argv[0]);
      exit(EXIT_FAILURE);
    }
  
  if(K) {
    stats_time("Generating complete components with Musor", "seconds", [&] () {
	g = cliques_with_edges(n, c, k);
      });
  } else if(E) {
    stats_time("Generating Erdos-Renyi components with Musor", "seconds", [&] () {
	g = erdos_renyi_with_edges(n, c, k);
      });
  } else if(A) {
    stats_time("Generating prefrential attachment components with Musor", "seconds", [&] () {
	g = pattachment_with_musor(n, c, k, o);
      });
  } else if(C) {
    stats_time("Generating citation network", "seconds", [&] () {
	g = n_sink_pattachment(n, c, o);
      });
  } else if(num_vertices(g)==0) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  
  cout << "Verts: " << num_vertices(g) << endl;
  cout << "Edges: " << num_edges(g) << endl;

  graph_t gnew(g);

  stats_time("Running data reduction DR", "DR seconds", [&] () {
      reduce_graph(gnew);
      cout << "DR-Verts: " << num_vertices(gnew) << endl;
      cout << "DR-Edges: " << num_edges(gnew) << endl;
    });

  if(p) graph_print(p, g);


  stats_time("Running max conn heuristics with interleaved DR", "MC-iDR seconds", [&] () {
      heurres = max_conn_heuristic(g);
      cout << "MC-iDR found k = " << heurres << endl;
    });

  gnew = g;
  stats_time("Running max conn heuristics with DR", "MC-DR seconds", [&] () {
      reduce_graph(gnew);
      int k = max_conn_heuristic_noDR(gnew);
      if(k < heurres) heurres = k;
      cout << "MC-DR found k = " << k << endl;
    });

  stats_time("Running max conn heuristics without DR", "MC seconds", [&] () {
      int k = max_conn_heuristic_noDR(g);
      if(k < heurres) heurres = k;
      cout << "MC found k = " << k << endl;
    });

  stats_time("Running max conn heuristics with extra DR", "MC-extra seconds", [&] () {
      int k = max_conn_heuristic_withDR(g);
      if(k < heurres) heurres = k;
      cout << "MC-extra found k = " << k << endl;
    });

  stats_time("Running search tree with interleaved DR", "ST-iDR seconds", [&] () {
      for(int tryk = 1; tryk <= heurres; ++tryk) {
	cout << "trying to find solution of size " << tryk << endl;
	if(search_tree(g, tryk)) {
	  cout << "ST-iDR found optimal k = " << foundk << endl;
	  exit(EXIT_SUCCESS);
	} else {
	  cout << "ST-iDR found k > " << tryk << endl;
	}
      }
    });
  
  // gnew = g;
  // stats_time("Running search tree with DR", "ST-DR seconds", [&] () {
  //     reduce_graph(gnew);
  //     if(search_tree_noDR(gnew, heurres))
  // 	cout << "ST-DR found optimal k = " << foundk << endl;
  //     else cout << "ST-DR found k > " << heurres-1 << endl;
  //   });


  // stats_time("Running search tree without DR", "ST seconds", [&] () {
  //     if(search_tree_noDR(g, heurres))
  // 	cout << "ST found optimal k = " << foundk << endl;
  //     else cout << "ST found k > " << heurres-1 << endl;
  //   });

  

  // stats_time("Trying to improve with search tree", [&] () {
  //     if(search_tree(g, k-1))
  // 	cout << "found k = " << foundk << endl;
  //     else cout << "found k > " << k-1 << endl;
  //   });

  // stats_time("Reducing graph", [&] () {
  //     cout << "("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << k << "\n";
  //     reduce_graph(g);
  //     cout << "("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges), k = " << k << "\n";
  //   });

  return(EXIT_SUCCESS);
}


