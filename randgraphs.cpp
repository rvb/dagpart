#include "graph.hpp"
#include <random>
#include <cassert>
#include <chrono>

graph_t n_sink_pattachment(int n, int c, int outdeg) {
  vector<v_t> vert(n + c);
  vector<int> degree(n + c, 0);
  default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());  

  graph_t g;

  for(int i = 0; i < n + c; ++i) {
    vert[i] = add_vertex(g);
    g[vert[i]].idx = i;
  }
  
  int totdeg = 0;
  for(int i_p = 0; i_p < n; ++i_p) {
    int i = c + i_p;
    for(int m = 1; m <= outdeg; ++m) {
      uniform_int_distribution<int> dest_gen(0, totdeg + i - 1);
      int dest = dest_gen(generator);
      int a = 0;
      for(int j_p = 0; j_p < i; ++j_p) {
	int j = j_p;
	int b = a + degree[j];
	if(dest >= a && dest <= b) {
	  auto e = edge(vert[i], vert[j], g);
	  if(!e.second) {
	    e = add_edge(vert[i], vert[j], g);
	    g[e.first].w = 1;
	    degree[j]++;
	    totdeg+=1;
	  }
	  break;
	}
	a = b + 1;
      }
    }
  }
  
  return g;
}

graph_t pattachment_with_musor(int n, int c, int k, int outdeg) {
  vector<v_t> vert(n*c);
  vector<int> degree(n*c, 0);

  graph_t g;
  default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());  
  uniform_int_distribution<int> uni_int(0, c - 1);
  uniform_int_distribution<int> uni_intn(0, n - 1);

  for(int i = 0; i < n*c; ++i) {
    vert[i] = add_vertex(g);
    g[vert[i]].idx = i;
  }

  for(int i = 0; i < k; ++i) {
  start:
    int c1 = uni_int(generator);
    int c2 = uni_int(generator);
    while (c1 == c2) c2 = uni_int(generator);

    int v1 = uni_intn(generator);
    int v2 = uni_intn(generator);
    while (v2 == v1) { v2 = uni_intn(generator); }

    if(v1 > v2) {
      if(edge(vert[c1*n+v1], vert[c2*n+v2], g).second) goto start;
      auto e = add_edge(vert[c1*n+v1], vert[c2*n+v2], g);
      g[e.first].w = 1;
    } else {
      if(edge(vert[c2*n+v2], vert[c1*n+v1], g).second) goto start;
      auto e = add_edge(vert[c2*n+v2], vert[c1*n+v1], g);
      g[e.first].w = 1;
    }
  }
  
  for(int comp = 0; comp < c; ++comp) {
    int totdeg = 0;
    for(int i_p = 1; i_p < n; ++i_p) {
      int i = comp*n + i_p;
      for(int m = 1; m <= ((i_p <= outdeg)?i_p:outdeg); ++m) {
	uniform_int_distribution<int> dest_gen(0, totdeg + i_p - 1);
	int dest = dest_gen(generator);
	int a = 0;
	for(int j_p = 0; j_p < i_p; ++j_p) {
	  int j = comp*n + j_p;
	  int b = a + degree[j];
	  if(dest >= a && dest <= b) {
	    auto e = edge(vert[i], vert[j], g);
	    if(!e.second) {
	      e = add_edge(vert[i], vert[j], g);
	      g[e.first].w = 1;
	      degree[j]++;
	      totdeg+=1;
	    }
	    break;
	    }
	  a = b + 1;
	}
      }
    }
  }
  
  return g;
}

graph_t cliques_with_edges(int n, int c, int k) {
  vector<v_t> vert(n*c);
  graph_t g;

  default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
  uniform_int_distribution<int> uni_int(0, c - 1);
  uniform_int_distribution<int> uni_intn(0, n - 1);

  for(int i = 0; i < n*c; ++i) {
    vert[i] = add_vertex(g);
    g[vert[i]].idx = i;
  }

  for(int i = 0; i < k; ++i) {
    int c1 = uni_int(generator);
    int c2 = uni_int(generator);
    while (c1 == c2) c2 = uni_int(generator);

    int v1 = uni_intn(generator);
    int v2 = uni_intn(generator);
    while (v2 == v1) { v2 = uni_intn(generator); }

    if(v1 > v2) {
      auto e = add_edge(vert[c1*n+v1], vert[c2*n+v2], g);
      g[e.first].w = 1;
    } else {
      auto e = add_edge(vert[c2*n+v2], vert[c1*n+v1], g);
      g[e.first].w = 1;
    }
  }

  for(int comp = 0; comp < c; ++comp) 
    for(int i = comp*n; i < (comp+1)*n; ++i)
      for(int j = i+1; j < (comp+1)*n; ++j) {
	auto e = add_edge(vert[j], vert[i], g);
	g[e.first].w = 1;
      }

  return g;
}


graph_t erdos_renyi_with_edges(int n, int c, int k) {
  vector<v_t> vert(n*c);
  graph_t g;

  default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
  uniform_int_distribution<int> uni_int(0, c - 1);
  uniform_int_distribution<int> uni_intn(0, n - 1);
  uniform_int_distribution<int> have_edge(0,1);

  for(int i = 0; i < n*c; ++i) {
    vert[i] = add_vertex(g);
    g[vert[i]].idx = i;
  }

  for(int i = 0; i < k; ++i) {
    int c1 = uni_int(generator);
    int c2 = uni_int(generator);
    while (c1 == c2) c2 = uni_int(generator);

    int v1 = uni_intn(generator);
    int v2 = uni_intn(generator);
    while (v2 == v1) { v2 = uni_intn(generator); }

    if(v1 > v2) {
      auto e = add_edge(vert[c1*n+v1], vert[c2*n+v2], g);
      g[e.first].w = 1;
    } else {
      auto e = add_edge(vert[c2*n+v2], vert[c1*n+v1], g);
      g[e.first].w = 1;
    }
  }

  for(int comp = 0; comp < c; ++comp) 
    for(int i = comp*n + 1; i < (comp+1)*n; ++i) {
      bool added = false;
      while(!added)
	for(int j = comp*n; j < i; ++j)
	  if(have_edge(generator)) {
	    auto e = add_edge(vert[i], vert[j], g);
	    g[e.first].w = 1;
	    added = true;
	  }
    }

  return g;
}

