#include <vector>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>
#include <list>
#include <graph.hpp>
#include <vector>
#include <boost/graph/topological_sort.hpp>

using namespace std;

graph_t graph_read(const char *fname) {
  graph_t g;
  unordered_map<unsigned, v_t> Vmap;
  string line;

  ifstream ifs(fname);
  while(getline(ifs,line))
  {
    if(line.empty())
      continue;
    istringstream iss(line);
    string s;
    unsigned a,b;
    iss >> a >> s >> b;

    v_t c, d;

    if(Vmap.find(a) == Vmap.end()) {
      c = add_vertex(g);
      Vmap[a]=c;
    } else c = Vmap[a];
    if(Vmap.find(b) == Vmap.end()) {
      d = add_vertex(g);
      Vmap[b]=d;
    } else d = Vmap[b];

    pair<e_t, bool> res;
    if(s == "->") res = add_edge(c,d,g);
    else if(s == "<-") res = add_edge(d,c,g);
    else assert(false);

    g[res.first].w = 1;
    g[res.first].deleted = 0;
  }
  cerr << "done ("<<num_vertices(g)<<" nodes, "<<num_edges(g)<<" edges)\n";

  v_iter vs, ve;
  tie(vs, ve) = vertices(g);
  for(int i = 0; vs != ve; ++vs, ++i) g[*vs].idx = i;

  return g;
}

void graph_print(const char *fname, const graph_t& g) {
  v_iter vs, ve;

  ofstream ofs(fname);
  
  ofs << "digraph G {" << endl;

  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs)
    ofs << g[*vs].idx << ";" << endl;

  e_iter es, ee;
  tie(es, ee) = edges(g);
  for(; es != ee; ++es)
    ofs << g[source(*es,g)].idx <<" -> "<< g[target(*es,g)].idx <<"[w="<<g[*es].w<< "];" << endl;

  ofs << "}" << endl;
}

void print_seedata(const graph_t& g, const vector<v_t> &seedata) {
  v_iter vs, ve;
  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs)
    if(seedata[g[*vs].idx]) 
      cout << "Vertex " << g[*vs].idx << " only sees " << g[seedata[g[*vs].idx]].idx << endl;
}

bool issink(const v_t &v, const graph_t &g) {
    out_iter es, ee;
    tie(es, ee) = out_edges(v, g);
    return es == ee;
}

list<v_t> sinks(const graph_t& g) {
  list<v_t> res;

  v_iter vs, ve;
  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs) {
    if(issink(*vs, g)) res.push_back(*vs);
  }

  return res;
}

void reduce_graph(graph_t &g) {
  vector<v_t> seedata(num_vertices(g), nullptr);
  vector<pair<bool, e_t> > edge_to(num_vertices(g));
  v_iter vs, ve;
  e_iter es, ee;

  /* delete deleted edges */

  tie(es, ee) = edges(g);
  for(auto next = es; es != ee; es = next) {
    ++next;
    if(g[*es].deleted) remove_edge(*es, g);
  }

  /* Compute the L-sets */
  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs)
    if(issink(*vs, g))
      seedata[g[*vs].idx] = *vs;
  
  list<v_t> sorted;
  topological_sort(g, back_inserter(sorted), vertex_index_map(get(&vertinfo::idx, g)));

  for(auto v = sorted.begin(); v != sorted.end(); ++v) {
    out_iter es, ee;
    tie(es, ee) = out_edges(*v, g);
    if(es == ee) continue;
    
    v_t u = target(*es++, g);
    seedata[g[*v].idx] = seedata[g[u].idx];
    
    for(; es != ee; ++es) {
      u = target(*es, g);
      if(seedata[g[u].idx] != seedata[g[*v].idx]) {
	seedata[g[*v].idx] = nullptr;
	break;
      }
    }
  }

  tie(vs, ve) = vertices(g);
  for(; vs != ve; ++vs) {
    // If *vs sees only the sink *vs, do not do anything
    if(seedata[g[*vs].idx] == *vs) continue;

    // *vs is in an L-set, remove all of its out-arcs
    if(seedata[g[*vs].idx] && (seedata[g[*vs].idx] != *vs)) {
      clear_out_edges(*vs, g);
      continue;
    }

    v_t u = *vs;

    // Initialize edge_to array: assume that we have no edges to any
    // sinks that we can be redirected to. the We need the edge-to
    // array to have the edge descriptors of our out-edges
    // random-accessible for weight increases
    out_iter es, ee;
    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es) {
      v_t v = target(*es, g);
      
      if(seedata[g[v].idx]) edge_to[g[seedata[g[v].idx]].idx] = make_pair(false, e_t());
    }

    // Now really add the edge descriptors to the sinks that we really
    // have edges to
    tie(es, ee) = out_edges(*vs, g);
    for(; es != ee; ++es) {
      edge_to[g[target(*es, g)].idx] = make_pair(true, *es);
    }
    
    // Now redirect edges from L-sets to sinks
    tie(es, ee) = out_edges(*vs, g);
    for(auto next = es; es != ee; es = next) {
      ++next;
      v_t v = target(*es, g);

      if(seedata[g[v].idx] && seedata[g[v].idx] != v) {
	// Add an edge to the sink of the L-set of v if it does not exist
	if(!edge_to[g[seedata[g[v].idx]].idx].first) {
	  auto res = add_edge(u, seedata[g[v].idx], g);
	  edge_to[g[seedata[g[v].idx]].idx] = make_pair(true, res.first);
	  g[edge_to[g[seedata[g[v].idx]].idx].second].w = 0;
	}

	// Increase weight of the edge into the sink.
	g[edge_to[g[seedata[g[v].idx]].idx].second].w += g[*es].w;

	// Edge into L-set gets removed
	remove_edge(*es, g);
      }
    }
  }

  tie(es, ee) = edges(g);
  for(; es != ee; ++es) {
    v_t u = target(*es, g);
    if(seedata[g[u].idx] && seedata[g[u].idx] == u)
       edge_to[g[u].idx] = make_pair(true, *es);
  }

  tie(vs, ve) = vertices(g);
  int newidx = 0;
  for(auto next = vs; vs != ve; vs = next) {
    ++next;
    if(seedata[g[*vs].idx] && seedata[g[*vs].idx] == *vs && !edge_to[g[*vs].idx].first)
      remove_vertex(*vs, g);
    else if(seedata[g[*vs].idx] && seedata[g[*vs].idx] != *vs)
      remove_vertex(*vs, g);
    else g[*vs].idx = newidx++;
  }
  //graph_print(g);
}
