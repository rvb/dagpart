/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2010 René van Bevern <m5bere2@uni-jena.de>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GRAPH_UTILS_H
#define _GRAPH_UTILS_H

#include <list>
#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

using namespace std;

using boost::vertices;
using boost::edges;
using boost::get;
using boost::vertex_index;
using boost::add_edge;
using boost::remove_edge;

struct vertinfo {
  int idx;
};

struct edgeinfo {
  int w;
  bool deleted;
};

/// graph type
typedef boost::adjacency_list<boost::listS, boost::listS, boost::directedS, vertinfo, edgeinfo> graph_t;

/// iterator over all graph vertices
typedef boost::graph_traits<graph_t>::vertex_iterator v_iter;

/// iterator over all graph edges
typedef boost::graph_traits<graph_t>::edge_iterator e_iter;

typedef boost::graph_traits<graph_t>::out_edge_iterator out_iter;

/// vertex type
typedef boost::graph_traits<graph_t>::vertex_descriptor v_t;

/// edge type
typedef boost::graph_traits<graph_t>::edge_descriptor e_t;

//typedef boost::property_map<graph_t, boost::vertex_index_t>::type i_map;

/// predicate type for edge filtering
/// typedef function2<bool, const graph_t&, e_t> e_filter;

/// predicate type for vertex filtering
/// typedef function2<bool, const graph_t&, v_t> v_filter;

/** remove edges in graph according to a given predicate
    @param G the graph to work on

    @param f an ::EdgeFilter function that is given the graph @a G and
    an edge. The function should return true if the edge
    should be removed from the graph, false otherwise.
    
    @post @a G has no edges satisfying @a f. */
/// graph_t& remove_edges(graph_t &G, const e_filter& f);

/** remove vertices in graph according to a given predicate
    @param G the graph to work on

    @param f a ::VertexFilter function that is given the graph @a G and
    the a vertex. The function should return true if the vertex
    should be removed from the graph, false otherwise.
    
    @post @a G has no edges satisfying @a f. */
/// graph_t& remove_vertices(graph_t &G, const v_filter& f);

graph_t graph_read(const char *fname);
void graph_print(const char*, const graph_t& g);
list <v_t> sinks(const graph_t& g);
void reduce_graph(graph_t& g);
bool issink(const v_t &v, const graph_t &g);
#endif
